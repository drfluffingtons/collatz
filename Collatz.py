#!/usr/bin/env python3

# ---------------------------
# projects/collatz/Collatz.py
# Copyright (C) 2016
# Glenn P. Downing
# ---------------------------

# ------------
# collatz_read
# ------------


def collatz_read(s):
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]

# dictionary for my lazy cache, storing number : cycle length


numStore = {}

# ------------
# collatz_eval
# ------------


def collatz_eval(i, j):
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
#    return 1

    # assertions for confirming that i and j are in the specified range

    assert(i > 0)
    assert(j > 0)

    assert(i < 1000000)
    assert(j < 1000000)

    # swap the nums if i is more than j so that the for in range will work

    if i > j:
        i, j = j, i

    # accounting for the case that i and j are equal

    elif i == j:
        sameCase = i
        sameLen = 1

        while sameCase > 1:
            if(sameCase % 2 == 0):
                sameCase = sameCase / 2
            else:
                sameCase = (sameCase * 3) + 1
            sameLen += 1

        return sameLen

    # now that I've confirmed they're not equal and maybe swapped i and j
    # assert that i is most certainly less than j

    assert(i < j)

    maxLen = 1

    for num in range(i, j + 1):
        cycleLen = 1
        colNum = num

        while colNum > 1:

            # first check if the num is in the cache dictionary
            # if so, add current and stored cycle lengths -1, break while loop
            if(colNum in numStore.keys()):
                cacheVal = numStore.get(colNum)
                cycleLen = cacheVal + cycleLen - 1
                break

            # if the number isn't in the dictionary, continue with collatz
            if(colNum % 2 == 0):
                colNum = colNum / 2
            else:
                colNum = (colNum * 3) + 1
            cycleLen += 1

        # store the max cycle length thus far in the loop
        if maxLen < cycleLen:
            maxLen = cycleLen

        # store the current number : cycle length in the dictionary for cache
        numStore[num] = cycleLen

    assert(type(maxLen) is int)

    return maxLen

# -------------
# collatz_print
# -------------


def collatz_print(w, i, j, v):
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")

# -------------
# collatz_solve
# -------------


def collatz_solve(r, w):
    """
    r a reader
    w a writer
    """
    for s in r:
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)
